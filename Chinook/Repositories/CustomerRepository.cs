﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chinook.Models;
using Microsoft.Data.SqlClient;

namespace Chinook.Repositories
{
    public class CustomerRepository:ICustomerRepository

    {
        /// <summary>
        /// Get all Cutomers 
        /// </summary>
        public  List<Customer> GetAllCustomers()
        {
            List<Customer> customers = new List<Customer>();
            string sql = "SELECT CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email FROM Customer";
            try
            {
                using (SqlConnection connection= new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command= new SqlCommand(sql,connection) )
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = SafeGetString(reader, "FirstName");
                                customer.LastName = SafeGetString(reader, "LastName");
                                customer.Country = SafeGetString(reader, "Country");
                                customer.PostalCode = SafeGetString(reader, "PostalCode");
                                customer.Phone = SafeGetString(reader, "Phone");
                                customer.Email = SafeGetString(reader, "Email");
                                customers.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
            }

            return customers;
        }
      
        /// <summary>
        /// Get Customer by id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public Customer GetCustomerById(int Id)
        {
            Customer customer = new Customer();
            string sql = "SELECT * FROM Customer WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@CustomerId", Id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                               
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = SafeGetString(reader, "FirstName");
                                customer.LastName = SafeGetString(reader, "LastName"); 
                                customer.Country = SafeGetString(reader, "Country");
                                customer.PostalCode = SafeGetString(reader, "PostalCode");
                                customer.Phone = SafeGetString(reader, "Phone");
                                customer.Email = SafeGetString(reader, "Email");
                              
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
            }

            return customer;
        }
        /// <summary>
        /// Add new Customer
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public bool AddCustomer(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO customer(FirstName,LastName,Country,PostalCode,Phone,Email)"+
                         " VALUES(@FirstName,@LastName,@Country,@PostalCode,@Phone,@Email)";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);
                        command.Parameters.AddWithValue("@Country", customer.Country);
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        command.Parameters.AddWithValue("@Phone", customer.Phone);
                        command.Parameters.AddWithValue("@Email", customer.Email);
                        success= command.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
            }

            return success;
        }

        /// <summary>
        /// Update Customer by id
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>

        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sql = "UPDATE Customer" +
                         " SET FirstName=@FirstName,LastName=@LastName,Country=@Country," +
                         "PostalCode=@PostalCode,Phone=@Phone,Email=@Email"+
                         " WHERE CustomerId=@CustomerId";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);
                        command.Parameters.AddWithValue("@Country", customer.Country);
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        command.Parameters.AddWithValue("@Phone", customer.Phone);
                        command.Parameters.AddWithValue("@Email", customer.Email);
                        success = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
            }

            return success;
        }
        /// <summary>
        /// Get specific customer by name
        /// </summary>
        /// <param name="FirstName"></param>
        /// <returns></returns>
        public List<Customer> GetSpecificCustomersName(string FirstName)
        {
            List<Customer> Specificcustomers = new List<Customer>();
            string sql = ("SELECT * FROM Customer WHERE (FirstName LIKE \'" + (@FirstName + "%\')"));

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@FirstName", FirstName);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = SafeGetString(reader, "FirstName");
                                customer.LastName = SafeGetString(reader, "LastName");
                                customer.Country = SafeGetString(reader, "Country");
                                customer.PostalCode = SafeGetString(reader, "PostalCode");
                                customer.Phone = SafeGetString(reader, "Phone");
                                customer.Email = SafeGetString(reader, "Email");
                                Specificcustomers.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
            }

            return Specificcustomers;
        }
        /// <summary>
        ///  Get Customer list by limit and offest
        /// </summary>
        /// <param name="Limit"></param>
        /// <param name="Ofset"></param>
        /// <returns></returns>
        public List<Customer> GetAllCustomersFromLimitOfset(int Limit,int Ofset)
        {
            List<Customer> customers = new List<Customer>();
            string sql = "SELECT  * FROM Customer ORDER BY CustomerId OFFSET (@OFFSET) ROWS FETCH NEXT (@LIMIT) ROWS ONLY";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@LIMIT", Limit);
                        command.Parameters.AddWithValue("@OFFSET", Ofset);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = SafeGetString(reader, "FirstName");
                                customer.LastName = SafeGetString(reader, "LastName");
                                customer.Country = SafeGetString(reader, "Country");
                                customer.PostalCode = SafeGetString(reader, "PostalCode");
                                customer.Phone = SafeGetString(reader, "Phone");
                                customer.Email = SafeGetString(reader, "Email");
                                customers.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
            }

            return customers;
        }
        /// <summary>
        /// Get Group customers by country
        /// </summary>
        /// <returns></returns>
        public List<CustomerCountry> GetAllCustomersByCountery()
        {
            List<CustomerCountry> customers = new List<CustomerCountry>();
            string sql = "SELECT Country, count(*) as CustomerCount from Customer GROUP BY Country ORDER BY CustomerCount DESC";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry customersInEachCountries = new CustomerCountry();
                                customersInEachCountries.Count = reader.GetInt32(1);
                                customersInEachCountries.Country = reader.GetString(0);
                                customers.Add(customersInEachCountries);

                               // Console.WriteLine("{0}  {1}", reader[0], reader[1]);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
            }

            return customers;
        }
        /// <summary>
        /// Get get high spenders by total invoice
        /// </summary>
        /// <returns></returns>
        public List<CustomerSpender> GetAllCustomersByHighestSpender()

        {
            List<CustomerSpender> customers = new List<CustomerSpender>();
            
            //string sql = "SELECT Customer.FirstName, Invoice.Total" +
            //" FROM Invoice INNER JOIN Customer ON Customer.CustomerId = Invoice.CustomerId" +
            //" ORDER BY Invoice.Total DESC";
            string sql = "SELECT Customer.FirstName, SUM(Invoice.Total) AS TotalItemsOrdered " +
                         " FROM Invoice, Customer " +
                         " Where Invoice.CustomerId = Customer.CustomerId Group by Customer.FirstName, Customer.LastName ORDER BY TotalItemsOrdered DESC";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender customerSpender = new CustomerSpender();
                                customerSpender.Total = reader.GetDecimal(1);
                                customerSpender.FirstName = reader.GetString(0);
                                customers.Add(customerSpender);
                               // Console.WriteLine("{0}  {1}", reader[0], reader[1]);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
            }

            return customers;
        }
        /// <summary>
        /// Get Popular genre correspond by track 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public CustomerGenre GetCustomerPopularGenre(int Id)
        {
            CustomerGenre customerGenre = new CustomerGenre();
            string sql = "SELECT TOP 1 Customer.FirstName,Genre.Name,COUNT(Genre.Name) AS GenreCount "+
            "FROM Customer "+
            "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId "+
            "INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId "+
            "INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId "+
            "INNER JOIN Genre ON Track.GenreId = Genre.GenreId "+
            "WHERE Customer.CustomerId = @CustomerId " +
            "GROUP BY Genre.Name , Customer.FirstName "+
            "ORDER BY GenreCount DESC";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@CustomerId", Id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                customerGenre.GenreCount = reader.GetInt32(2);
                                customerGenre.FirstName = SafeGetString(reader, "FirstName");
                                customerGenre.GenreName = SafeGetString(reader, "Name");
                                //Console.WriteLine("{0}  {1}", reader[0], reader[1]);

                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
            }

            return customerGenre;
        }
        /// <summary>
        /// Extention method to handel null value
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="colName"></param>
        /// <returns></returns>
        public static string SafeGetString(SqlDataReader reader, string colName)
        {
            int colIndex = reader.GetOrdinal(colName);

            if (!reader.IsDBNull(colIndex))
            {
                return reader.GetString(colIndex);
            }
            else
            {
                return null;
            }
        }
    }

}
