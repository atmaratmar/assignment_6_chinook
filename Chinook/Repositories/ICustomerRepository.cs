﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chinook.Models;

namespace Chinook.Repositories
{
   public interface ICustomerRepository
   {
       public List<Customer> GetAllCustomers();
       public Customer GetCustomerById(int Id);
       public bool AddCustomer(Customer customer);
       public bool UpdateCustomer(Customer customer);
       public List<Customer> GetSpecificCustomersName(string FirstName);
       public List<Customer> GetAllCustomersFromLimitOfset(int Limit, int Ofset);
       public List<CustomerCountry> GetAllCustomersByCountery();
       public List<CustomerSpender> GetAllCustomersByHighestSpender();
       public CustomerGenre GetCustomerPopularGenre(int Id);

   }
}
