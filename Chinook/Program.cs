﻿using System;
using System.Collections.Generic;
using System.Threading.Channels;
using Chinook.Models;
using Chinook.Repositories;

namespace Chinook
{
    class Program
    {
        static void Main(string[] args)
        {
         
            ICustomerRepository repository = new CustomerRepository();
            ////---------------------------------------------------------//
            //TestGetAllCustomers(repository);
            ////---------------------------------------------------------//
            //TestSelectSpecificCustomerById(repository);
            ////---------------------------------------------------------//
            //TestUpdateCustomer(repository);
            ////---------------------------------------------------------//
            //TestInsetCustomer(repository);
            //// --------------------------------------------------------//
            //TestSelectSpecificCustomerByName(repository);
            ////---------------------------------------------------------//
            //TestGetAllCustomersFromLimitOfset(repository);
            ////---------------------------------------------------------//
            //TestGetAllCustomersByCountery(repository);
            ////---------------------------------------------------------//
            //TestGetAllCustomersByHighestSpender(repository);
            ////---------------------------------------------------------//
            //TestSelectSpecificGenre(repository);

        }

        static void TestGetAllCustomers(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomers());
        }
        static void TestGetAllCustomersFromLimitOfset(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomersFromLimitOfset(10, 5));
        }
        static void TestSelectSpecificCustomerById(ICustomerRepository repository)
        {

            PrintCustomer(repository.GetCustomerById(10));
        }
        static void TestSelectSpecificCustomerByName(ICustomerRepository repository)
        {

            PrintCustomers(repository.GetSpecificCustomersName("at"));
        }
        static void TestGetAllCustomersByCountery(ICustomerRepository repository)
        {
            PrintCustomerCountries(repository.GetAllCustomersByCountery());
        }
        static void TestGetAllCustomersByHighestSpender(ICustomerRepository repository)
        {
            PrintCustomerSpender(repository.GetAllCustomersByHighestSpender());
        }
        static void TestInsetCustomer(ICustomerRepository repository)
        {
            Customer test = new Customer()
            {
                FirstName = "jazmin",
                LastName = "kohistany",
                PostalCode = "121111",
                Country = "denmark",
                Phone = "21121212",
                Email = "a@.com"

            };
            if (repository.AddCustomer(test))
            {
                Console.WriteLine("registerd");
            }
            else
            {
                Console.WriteLine("something  went wrong");
            }
        }
        static void TestUpdateCustomer(ICustomerRepository repository)
        {
            Customer test = new Customer()
            {
                CustomerId = 60,
                FirstName = "jazmin",
                LastName = "kohistany",
                PostalCode = "121111",
                Country = "denmark",
                Phone = "21121212",
                Email = "a@.com"

            };
            if (repository.UpdateCustomer(test))
            {
                Console.WriteLine("Updated the customer");
            }
            else
            {
                Console.WriteLine("Something went wrong");
            }

        }

        static void TestSelectSpecificGenre(ICustomerRepository repository)
        {

            PrintCustomerGere(repository.GetCustomerPopularGenre(1));
        }

        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

         static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"{customer.CustomerId} {customer.FirstName}  {customer.LastName}  {customer.PostalCode}  {customer.Country}  " +
                              $"{customer.Phone}  {customer.Email}");
        }

         static void PrintCustomerCountries(IEnumerable<CustomerCountry> customers)
         {
             foreach (CustomerCountry customer in customers)
             {
                 PrintCustomer(customer);
             }
         }

         static void PrintCustomer(CustomerCountry customer)
         {
             Console.WriteLine($"{customer.Country} {customer.Count}");
         }
         static void PrintCustomerSpender(IEnumerable<CustomerSpender> customers)
         {
             foreach (CustomerSpender customer in customers)
             {
                 PrintCustomer(customer);
             }
         }

         static void PrintCustomer(CustomerSpender customer)
         {
            Console.WriteLine($"{customer.FirstName }   { customer.Total}");

         }
         static void PrintCustomerGere(CustomerGenre customer)
         {
             Console.WriteLine($"{customer.FirstName}  {customer.GenreName}  {customer.GenreCount}");
         }

    }
}